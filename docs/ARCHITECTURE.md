# File structure

The file structure is a healthy mix of proposed project layout by https://github.com/golang-standards/project-layout
and the structure used by Astilectron library. Because the application seems to be non-standard,
the explanation may be important.

### `/api`
An OpenAPI specification of the internal back-end web API. The API is used to communicate with the front-end.

### `/cmd`
A standard Go directory for starting entry-points of the application. The folder contains folders named by a tool
or an app that resides inside. In each of them should be a sole `main.go` file in the `main` package.
By compiling just a single `main.go` file from this structure you can create an app that can use all the code
without a need to duplicate it. For example, you can run just a web API without UI for e2e tests.

There is one exception to this rule - the main `main.go` file. It is the most important entry-point that creates
a full working application, which is then packaged. The file resides in the root directory of the project.

The `main.go` files should be as small as possible. Only quick bootstrap, config and run the needed packages.

### `/config`
The application config folder.

### `/docs`
A folder for human-readable documentation of the project. This file is in it.

### `/internal`
A standard Go directory for internal, application specific Golang code. Every code that shouldn't be reusable
by other projects have to be in this directory.

### `/output`
Here lie runnable output files as a result of a project compilation.
Inside there are folders for each platform-architecture mix, eg. `windows-amd64`. The executable files are in them.
In the repo the folder should remain empty or better - non-existent. Please don't push output files.

### `/pkg`
Packages (Go files) that can be reused outside the project.
It's like a `vendor` folder, but for a code created for the project.
If a package get to this folder there should be a possibility to use it without shenanigans.

### `/resources`
A directory for a non-Golang code that need to be compiled into the application.
That means icons, graphics, front-end, etc.

### `/tests`
A directory for e2e tests.

# Astilectron 

Application uses `go-astilectron` as a rendering library. Astilectron, on the other hand, uses Electron.
It means that almost all the front-end can be created as a JavaScript application,
which UI is constructed with HTML and CSS. The sole exceptions are the application windows itself (duh, Sherlock!)
and main menu. But Astilectron takes care of these aspects either.