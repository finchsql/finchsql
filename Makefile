# When the makefile grow too big, consider extracting bigger commands to the `scripts` folder.

.PHONY: build
build: setup clean-output
	rm -f bind.go
	astilectron-bundler -c config/bundler-local.json
	make clean

.PHONY: build-all
build-all: setup clean-output
	rm -f bind.go
	astilectron-bundler -c config/bundler.json
	make clean

setup: bind.go
	go get -u github.com/asticode/go-astilectron-bundler/...
	go install github.com/asticode/go-astilectron-bundler/astilectron-bundler
	@echo '# This file indicate that the dev env setup is completed. If not, remove it and execute `make setup`.' > setup

.PHONY: test
test:
	go test -race .

.PHONY: clean
clean:
	rm -f bind*.go
	rm -f windows.syso
	make bind.go

.PHONY: clean-output
clean-output:
	rm -f finchsql
	rm -rf output/*

bind.go:
	cp config/bind.go.placeholder bind.go
