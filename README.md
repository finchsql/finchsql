[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)
# FinchSQL

FinchSQL is a database client and management cross-platform application.

## Usage

To do

## Contributing

Contributions are always welcome!
You can post a bug report as an issue or
create a pull request with some changes.

Or you can join our community on Discord
and develop the application as the insider.

## Building from source

To clone the project:

```bash
  git clone https://gitlab.com/finchsql/finchsql
  cd finchsql
  make setup
```

Make sure all tests and static code analysis pass:

```bash
  make test
```

To build for your platform:

```bash
  make build
```

To build for all the platforms:

```bash
  make build-all
```
## License

[MIT](https://choosealicense.com/licenses/mit/)

## 🔗 Links

[![gitlab](https://img.shields.io/badge/gitlab-000?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/finchsql/finchsql)
[![discord](https://img.shields.io/badge/discord-0A66C2?style=for-the-badge&logo=discord&logoColor=white)](https://discord.gg/dSrzkgMD)

